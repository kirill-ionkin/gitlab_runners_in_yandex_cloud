FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine  AS build
WORKDIR /src

# Copy projects and restore dependencies
COPY *.csproj ./
RUN dotnet restore

# Copy everything else
COPY . ./

# Build app
RUN dotnet publish -c release -o /publish

# Final stage
FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine AS runtime
COPY --from=build /publish ./app
WORKDIR /app
ENTRYPOINT ["dotnet", "app-example.dll"]
